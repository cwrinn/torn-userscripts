// ==UserScript==
// @name         Mog's Super Aggressive Hospital Filter
// @namespace    http://www.mogsrealm.com/
// @version      2024-08-15
// @description  Filters out Revives Unavailable, like everyone else, but ALSO Attacks/Mugs/Hosps/Losses/Self-Hosps. Don't interfere in wars you're not paid for!
// @author       LordMogra
// @match        https://www.torn.com/hospitalview.php
// @icon         https://www.google.com/s2/favicons?sz=64&domain=torn.com
// @updateURL    https://gitlab.com/lordmogra/torn-userscripts/-/raw/main/aggressivehospfilter.user.js
// @downloadURL  https://gitlab.com/lordmogra/torn-userscripts/-/raw/main/aggressivehospfilter.user.js
// @grant        none
// ==/UserScript==

(function($) {
    'use strict';
    function waitForElm(selector) {
        return new Promise(resolve => {
            if (document.querySelector(selector)) {
                return resolve(document.querySelector(selector));
            }

            const observer = new MutationObserver(mutations => {
                if (document.querySelector(selector)) {
                    observer.disconnect();
                    resolve(document.querySelector(selector));
                }
            });

            // If you get "parameter 1 is not of type 'Node'" error, see https://stackoverflow.com/a/77855838/492336
            observer.observe(document.body, {
                childList: true,
                subtree: true
            });
        });
    }

    const toggleButton = $('<button type="button" class="torn-btn">Toggle revive filter</button>');
    const icon = $('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!--!Font Awesome Free 6.6.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path fill="#ffffff" d="M495.9 166.6c3.2 8.7 .5 18.4-6.4 24.6l-43.3 39.4c1.1 8.3 1.7 16.8 1.7 25.4s-.6 17.1-1.7 25.4l43.3 39.4c6.9 6.2 9.6 15.9 6.4 24.6c-4.4 11.9-9.7 23.3-15.8 34.3l-4.7 8.1c-6.6 11-14 21.4-22.1 31.2c-5.9 7.2-15.7 9.6-24.5 6.8l-55.7-17.7c-13.4 10.3-28.2 18.9-44 25.4l-12.5 57.1c-2 9.1-9 16.3-18.2 17.8c-13.8 2.3-28 3.5-42.5 3.5s-28.7-1.2-42.5-3.5c-9.2-1.5-16.2-8.7-18.2-17.8l-12.5-57.1c-15.8-6.5-30.6-15.1-44-25.4L83.1 425.9c-8.8 2.8-18.6 .3-24.5-6.8c-8.1-9.8-15.5-20.2-22.1-31.2l-4.7-8.1c-6.1-11-11.4-22.4-15.8-34.3c-3.2-8.7-.5-18.4 6.4-24.6l43.3-39.4C64.6 273.1 64 264.6 64 256s.6-17.1 1.7-25.4L22.4 191.2c-6.9-6.2-9.6-15.9-6.4-24.6c4.4-11.9 9.7-23.3 15.8-34.3l4.7-8.1c6.6-11 14-21.4 22.1-31.2c5.9-7.2 15.7-9.6 24.5-6.8l55.7 17.7c13.4-10.3 28.2-18.9 44-25.4l12.5-57.1c2-9.1 9-16.3 18.2-17.8C227.3 1.2 241.5 0 256 0s28.7 1.2 42.5 3.5c9.2 1.5 16.2 8.7 18.2 17.8l12.5 57.1c15.8 6.5 30.6 15.1 44 25.4l55.7-17.7c8.8-2.8 18.6-.3 24.5 6.8c8.1 9.8 15.5 20.2 22.1 31.2l4.7 8.1c6.1 11 11.4 22.4 15.8 34.3zM256 336a80 80 0 1 0 0-160 80 80 0 1 0 0 160z"/></svg>');
    icon.css('margin', '9px 11px 0px 10px')
        .css('height', '18px');
    const infoBlock = $("<div class='info-msg-cont border-round m-top10'><div class='info-msg border-round'><div class='delimiter'><div class='msg right-round' tabindex='0'><p id='toggleBlock'></p></div></div></div></div>");
    infoBlock.find('.info-msg').prepend(icon);
    infoBlock.find('#toggleBlock').append(toggleButton);

    toggleButton.on('click', () =>
    {
        localStorage.setItem('torn_hospital_hide_rows', !(localStorage.getItem('torn_hospital_hide_rows') === 'true'));
        toggleFiltering();
    });

    function toggleFiltering()
    {
        waitForElm('.userlist-wrapper.hospital-list-wrapper .users-list .time').then(() =>
        {
            const rows = $(".reason:contains('Hospitalized'),"
                         + ".reason:contains('Mugged'),"
                         + ".reason:contains('Attacked'),"
                         + ".reason:contains('Lost'),"
                         + ".reason:contains('Ipecac'),"
                         + ".reason:contains('hemolytic'),"
                         + ".reviveNotAvailable")
            .parents('.users-list li');

            if (localStorage.getItem('torn_hospital_hide_rows') === 'true')
            {
                rows.hide();
                infoBlock.addClass('green').removeClass('red');
                toggleButton.html('Disable revive filter');
            }
            else
            {
                rows.show();
                infoBlock.addClass('red').removeClass('green');
                toggleButton.html('Enable revive filter');
            }
        });
    }

    toggleFiltering();
    if ($('#toggleBlock').length === 0)
    {
        infoBlock.insertBefore('.userlist-wrapper');
        $(window).on('hashchange', () =>
        {
            toggleFiltering();
        });
    }
})(jQuery);
