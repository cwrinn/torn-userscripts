// ==UserScript==
// @name         Quick Send FacVault Money
// @namespace    http://mogsrealm.com/
// @version      2024-06-21
// @description  Auto populate member and balance
// @author       LordMogra
// @match        *.torn.com/factions.php?step=your*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=torn.com
// @updateURL    https://gitlab.com/lordmogra/torn-userscripts/-/raw/main/quickSendFacVault.user.js
// @downloadURL  https://gitlab.com/lordmogra/torn-userscripts/-/raw/main/quickSendFacVault.user.js
// @grant        none
// ==/UserScript==

(function($) {
    'use strict';

    const isMobile = /Mobi/.test(navigator.userAgent);

    function waitForElm(selector) {
        return new Promise(resolve => {
            if (document.querySelector(selector)) {
                return resolve(document.querySelector(selector));
            }

            const observer = new MutationObserver(mutations => {
                if (document.querySelector(selector)) {
                    observer.disconnect();
                    resolve(document.querySelector(selector));
                }
            });

            // If you get "parameter 1 is not of type 'Node'" error, see https://stackoverflow.com/a/77855838/492336
            observer.observe(document.body, {
                childList: true,
                subtree: true
            });
        });
    }


    function checkInGiveToUser(event)
    {
        const inControls = window.location.hash.match(/tab=controls/);
        const inGiveToUser = window.location.hash.match(/option=give-to-user/) || !window.location.hash.includes('option');
        if (inControls && inGiveToUser)
        {
            waitForElm('.money-depositors li').then((elm) => {
                const moneyDepositors = $('.money-depositors');
                const depositors = moneyDepositors.children('li');
                const mu = $('#money-user');
                const mb = $('.money-wrap .input-money');
                const gm = $('#give-money');

                depositors.each(function (i){
                    const depositor = $(this);
                    const money = depositor.find('.amount > .show > .money').get(0)
                    const balance = money?.dataset.value;
                    if (balance > 0)
                    {
                        const autoDeposit = $('<span />',
                        {
                            class: 'autoDeposit',
                            css:
                            {
                                'background-position': '-43px -78px',
                                'display': isMobile ? 'inline-block' : 'none',
                            },
                        }).addClass('actionIcon').mouseenter(function()
                        {
                            $(this).css('cursor', 'pointer')
                                   .css('background-position', '-77px -78px');
                        }).mouseleave(function()
                        {
                            $(this).css('cursor', 'auto')
                                   .css('background-position', '-43px -78px');
                        }).click(function()
                        {
                            const username = depositor.find('a.user.name').first();
                            const fullUser = username.dataset ? username.dataset.placeholder : username.attr('title');
                            mu.val(fullUser).focus().trigger('input');

                            mb.val(balance).trigger('input').focus();
                            $('#money-user-cont').removeClass('ac-focus-input');

                            gm.prop('checked', true).change();
                        });
                        depositor.mouseenter(function()
                        {
                            isMobile || autoDeposit.css('display', 'inline-block');
                        }).mouseleave(function()
                        {
                            isMobile || autoDeposit.css('display', 'none');
                        });
                        const show = depositor.find('.amount > .show');
                        depositor.find('.autoDeposit').length || show.append(autoDeposit);
                    }
                });
            });
        }
    }
    window.addEventListener("hashchange", checkInGiveToUser);
    checkInGiveToUser();
})(jQuery);