// ==UserScript==
// @name         Update Employee Pay
// @namespace    http://mogsrealm.com/
// @version      0.1
// @description  Automatically update Employee Pay
// @author       LordMogra
// @match        https://www.torn.com/companies.php
// @icon         https://www.google.com/s2/favicons?domain=torn.com
// @grant        none
// ==/UserScript==

(() =>
{
  'use strict';

  (function(e,f){var b={},g=function(a){b[a]&&(f.clearInterval(b[a]),b[a]=null)};e.fn.waitUntilExists=function(a,h,j){var c=this.selector,d=e(c),k=d.not(function(){return e(this).data("waitUntilExists.found")});"remove"===a?g(c):(k.each(a).data("waitUntilExists.found",!0),h&&d.length?g(c):j||(b[c]=f.setInterval(function(){d.waitUntilExists(a,h,!0)},500)));return d}})(jQuery,window);

  $('.employee-list-wrap').waitUntilExists((event, xhr, settings) =>
  {
    for (const row of document.querySelectorAll('.employee-list-wrap .employee-list > li'))
    {
        const days = parseInt(row.querySelector('.days').textContent.match(/\d+/)[0]);
        const pay = 100000 + (days * 100);

        let lastValue = 0;
        const input = row.querySelector('.pay input[type=text]');
        input.value = pay;

        let event = new Event('input', { bubbles: true });
        // hack React15
        event.simulated = true;
        // hack React16
        let tracker = input._valueTracker;
        if (tracker) {
            tracker.setValue(lastValue);
        }
        input.dispatchEvent(event);

    }
  });
})();
